"""Definitions of final models + final utility functions"""
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from sklearn.metrics import (roc_auc_score,
                             roc_curve,
                             classification_report,
                             fbeta_score,
                             confusion_matrix)
from sklearn.pipeline import Pipeline
from sklearn.compose import make_column_selector

from sklearn_extensions.encoders import CustomOrdinalEncoder, UnknownCategoriesRemover
from sklearn_extensions.long_tail_remover import LongTailRemover
from sklearn_extensions.composers import CustomColumnTransformer
from sklearn_extensions.scalers import CustomStandardScaler
from modeling.custom_models import CustomCatBoostClassifier
from modeling.model_validation import optimize_metric

## cb_model
def make_cb_model(params):
    cat_transformer = Pipeline(steps = [("unknown_remover", UnknownCategoriesRemover()),
                                        ("lt_remover", LongTailRemover(threshold = 0.01)),
                                        ("oe", CustomOrdinalEncoder())
                                       ]
                               )
    num_transformer = Pipeline(steps = [("scaler", CustomStandardScaler())])
   
    type_preprocessor = CustomColumnTransformer(
        transformers = [("num", num_transformer, make_column_selector(dtype_exclude = "category")),
                        ("cat", cat_transformer, make_column_selector(dtype_include = "category"))],
        verbose_feature_names_out = False
        )

    cb_model = Pipeline(steps = [("preproc", type_preprocessor),
                                 ("classifier", CustomCatBoostClassifier(es_enabled = False,
                                                                         auto_class_weights = "Balanced",
                                                                         random_seed = 1993,
                                                                         **params))
                                 ]
                       )

    return cb_model

def runs_to_df(runs) -> pd.DataFrame:
    lst = []
    for r in runs:
        dct = r.data.metrics
        dct.update(r.data.params)
        dct.update({"run_name" : r.data.tags.get("mlflow.runName")})
        lst.append(dct)
    return pd.DataFrame(lst).set_index("run_name")

def get_run_params(df_runs : pd.DataFrame, idx : str) -> dict:
    dct = df_runs.loc[[idx]] \
        .filter(regex = "classifier") \
        .to_dict("records")[0]
    params = {k.replace("classifier__", ""):v for k,v in dct.items()}
    params.update(iterations = np.round(df_runs.loc[idx, "estop_iter_mean"]))
    keys = []
    values = []
    for k,v in params.items():
        keys.append(k)
        try:
            values.append(float(v))
        except:
            values.append(v)
    return dict(zip(keys, values))

def plot_roc(y_true_list, y_scores_list, names):
    fig = go.Figure()
    fig.add_shape(
        type='line', line=dict(dash='dash'),
        x0=0, x1=1, y0=0, y1=1
    )
    
    for i in range(len(y_scores_list)):
        y_true = y_true_list[i]
        y_score = y_scores_list[i]
    
        fpr, tpr, _ = roc_curve(y_true, y_score)
        auc_score = roc_auc_score(y_true, y_score)
    
        name = f"{names[i]} (AUC={auc_score:.4})"
        fig.add_trace(go.Scatter(x=fpr, y=tpr, name=name, mode='lines'))
    
    fig.update_layout(
        xaxis_title='False Positive Rate',
        yaxis_title='True Positive Rate',
        yaxis=dict(scaleanchor="x", scaleratio=1),
        xaxis=dict(constrain='domain'),
        width=700, height=500
    )
    return fig

def validate_model(y_train, y_test, model_preds, model_name):

    print("-"*50)
    print(model_name)

    f1_val, cutoff = optimize_metric(y_train, model_preds["train"], func = fbeta_score, beta = 1)
    print(f"Max train F1 score = {f1_val} for cutoff = {cutoff}")

    y_pred_train = np.where(model_preds["train"] >= cutoff, 1, 0)
    y_pred_test = np.where(model_preds["test"] >= cutoff, 1, 0)

    print("train:\n")
    print(confusion_matrix(y_true = y_train, y_pred = y_pred_train, labels = [0, 1]))
    print(classification_report(y_true = y_train, y_pred = y_pred_train, digits = 3))

    print("test:\n")
    f1_val = fbeta_score(y_true = y_test, y_pred = y_pred_test, beta = 1)
    print(f"test F1 score = {f1_val}")
    print(confusion_matrix(y_true = y_test, y_pred = y_pred_test, labels = [0, 1]))
    print(classification_report(y_true = y_test, y_pred = y_pred_test, digits = 3))
    ## plot roc
    fig = plot_roc(y_true_list = [y_train, y_test],
                   y_scores_list = [model_preds["train"], model_preds["test"]],
                   names = ["train", "test"])
    fig = fig.update_layout(title = f"{model_name} AUC train/test")
    fig.show()

def plot_mean_abs_shap(shap_values, cols):
    df = pd.DataFrame({"feature" : cols,
                       "feature_importance" : abs(shap_values).mean(axis = 0)}
                     )
    fig = px.bar(df.sort_values("feature_importance", ascending = True),
                 x = "feature_importance",
                 y = "feature",
                 orientation = "h")
    fig = fig.update_layout(title = "Feature importance by mean absolute SHAP value")
    fig.show()
