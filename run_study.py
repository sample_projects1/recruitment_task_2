"""Script for running optuna studies"""
import argparse
import sys
import optuna
import mlflow
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.model_selection import StratifiedKFold

from modeling.model_building import ModelBuilder
from modeling.model_validation import CrossValidator, validate_model
from modeling.model_optimization import Objective
from utils.rw_utils import load_joblib, load_yaml
from utils.time_utils import current_hour

parser = argparse.ArgumentParser(description = "Script for running optuna studies with integrated mlflow")
parser.add_argument("CONFIG",
                    type = str,
                    help = "Path to YAML configuration file")

if __name__ == "__main__":
    print("{h} - BEGINNING OF THE SCRIPT\n".format(h = current_hour()))
    t0 = datetime.now().replace(microsecond = 0)

    args = parser.parse_args()
    print(args)

    ## load config and import model
    config = load_yaml(args.CONFIG)
    exec(f"""from model_definitions import {config["model"]["name"]} as model""")

    ## load data
    df_train = load_joblib(f"./data/df_train.joblib")
    X = df_train.drop(columns = "classLabel")
    if config["data"]["remove_cols"]:
        X = X.drop(columns = config["data"]["remove_cols"])
        print(f'DROPPING FOLLOWING FEATURES: {config["data"]["remove_cols"]}')
    y = df_train["classLabel"]

    ## prepare model and CV
    mb = ModelBuilder(model_definition = model,
                      early_stopping = config["model"]["early_stopping"],
                      early_stopping_param = config["model"]["early_stopping_param"])
    CV = CrossValidator(X = X,
                        y = y,
                        cv = StratifiedKFold(n_splits = 10, shuffle = True, random_state = 1993),
                        validating_func = validate_model)

    ## study
    mlflow.set_tracking_uri(config["mlflow"]["tracking_uri"])
    mlflow.set_experiment(config["mlflow"]["experiment_name"])

    study = optuna.create_study(direction = config["optuna"]["direction"],
                                sampler = optuna.samplers.TPESampler(n_startup_trials = config["optuna"]["n_random"]),
                                study_name = config["optuna"]["run_name"],
                                storage = config["optuna"]["storage"],
                                load_if_exists = config["optuna"]["load_if_exists"])

    objective = Objective(model_builder = mb,
                          cross_validator = CV,
                          optim_params = config["model"]["optim_params"],
                          fixed_params = config["model"]["fixed_params"],
                          run_name = config["mlflow"]["run_name"],
                          config_path = args.CONFIG)

    study.optimize(objective, timeout = config["optuna"]["max_time_minutes"] * 60)

    t1 = datetime.now().replace(microsecond = 0)
    print("\n{h} - TOTAL EXECUTION TIME: {x}".format(h = current_hour(), x = t1 - t0))
else:
    sys.exit(0)
