""""Module with data exploration utility functions"""
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from typing import Union
from sklearn.metrics import roc_auc_score

from sklearn_extensions.variable_selection import calc_predictive_power
from sklearn_extensions.imputers import CustomSimpleImputer

def calc_num_summary(df : pd.DataFrame) -> pd.DataFrame:
    """Calculates summary of numerical features"""
    summary = df \
        .describe(include = "number", percentiles = [0.25, 0.5, 0.75, 0.95]) \
        .drop(["count", "std", "min", "max"], axis = 0) \
        .T
    return summary

def calc_cat_summary(df : pd.DataFrame) -> pd.DataFrame:
    """Calculates summary of categorical features"""
    summary = df \
        .describe(include = "object") \
        .drop(["count"], axis = 0) \
        .T
    summary["freq"] = summary["freq"]/df.shape[0]
    return summary

def _color_red(val : Union[int, float]) -> str:
    """Helper function for coloring pd.DataFrame"""
    if val <= 0.25:
        color = '' 
    elif val > 0.25 and val < 0.5:
        color = 'orange'
    else:
        color = 'red'
    return 'background-color: %s' % color

def compare_num_distr(df1 : pd.DataFrame,
                      df2 : pd.DataFrame):
    """Compares distributions of numerical variables between two df"""
    df1_summary = calc_num_summary(df1)
    df1_summary.columns = [c + "_1" for c in df1_summary.columns]
    df2_summary = calc_num_summary(df2)
    df2_summary.columns = [c + "_2" for c in df2_summary.columns]

    df_joined = df1_summary.join(df2_summary)
    prefix_list = ["mean", "25%", "50%", "75%", "95%"]

    for c in prefix_list:
        df_joined[f"{c}_diff"] = abs((df_joined[f"{c}_2"] - df_joined[f"{c}_1"]) / df_joined[f"{c}_1"])

    cols_list = sorted(df_joined.columns.tolist())
    df_joined = df_joined \
        .filter(cols_list) \
        .style.applymap(_color_red, subset = [c + "_diff" for c in prefix_list])
    return df_joined

def compare_cat_distr(df1 : pd.DataFrame,
                      df2 : pd.DataFrame):
    """Compares distributions of categorical variables between two df"""
    df1_summary = calc_cat_summary(df1)
    df1_summary.columns = [c + "_1" for c in df1_summary.columns]
    df2_summary = calc_cat_summary(df2)
    df2_summary.columns = [c + "_2" for c in df2_summary.columns]

    df_joined = df1_summary.join(df2_summary)
    prefix_list = ["unique", "top", "freq"]

    for c in ["unique", "top"]:
        df_joined[f"{c}_diff"] = (df_joined[f"{c}_2"] != df_joined[f"{c}_1"]).astype(int)
    df_joined["freq_diff"] = abs((df_joined["freq_2"] - df_joined["freq_1"]) / df_joined["freq_1"])

    cols_list = sorted(df_joined.columns.tolist())
    df_joined = df_joined \
        .filter(cols_list) \
        .style.applymap(_color_red, subset = [c + "_diff" for c in prefix_list])
    return df_joined

def explore_num(df, var, target, censor_q = 0.995):

    df_copy = df[[target, var]].copy()
    
    ## uncensored summary
    summary_uncensored = df_copy.groupby(target)[var].agg(["mean", "median"])

    ## censoring
    censor_q_value = df_copy[var].quantile(q = [censor_q]).iloc[0]
    df_copy["censored"] = df_copy[var]
    df_copy["censored"].loc[df_copy[var] > censor_q_value] = censor_q_value
    summary_censored = df_copy.groupby(target)["censored"].agg(["mean", "median"])

    ## boxplot
    boxplot = px.box(df_copy, x = target, y = "censored", title = f"Boxplot {var} (censored) vs {target}")
    boxplot = boxplot.update_layout(yaxis_title = var)

    ## decile plot
    mean_y = np.round(df_copy[target].mean(), 4)
    df_copy["var_cat"] = pd.qcut(df_copy[var], q = 10, labels = False, duplicates = "drop")
    df_copy["var_cat"] = df_copy["var_cat"].astype("category")
    
    if any(df_copy["var_cat"].isna()):
        df_copy["var_cat"] = df_copy["var_cat"].cat.add_categories("NO_DATA").fillna("NO_DATA")

    df_plot = df_copy \
        .assign(cnt = 1) \
        .groupby("var_cat") \
        .agg({target : "sum", "cnt" : "count"}) \
        .assign(perc = lambda x: np.round(x[target] / x["cnt"], 4)) \
        .reset_index()
    rename_dict = dict(zip(list(range(10)), ["d" + str(i) for i in range(10)]))
    df_plot["var_cat"] = df_plot["var_cat"].cat.rename_categories(rename_dict)

    decplot = go.Figure()
    decplot.add_trace(
        go.Scatter(x = df_plot["var_cat"],
                   y = df_plot["perc"],
                   name = "target = 1",
                   mode = "lines+markers+text",
                   text = df_plot["perc"],
                   textposition = "top center",
                   line = dict(color = "navy", width = 3))
    )
    decplot.add_trace(
        go.Scatter(x = df_plot["var_cat"],
                   y = [mean_y for i in range(11)],
                   name = "avg target",
                   mode = "lines",
                   line = dict(color = "gray", width = 3, dash = "dash"))
    )
    decplot = decplot.update_layout(title = f"Avg {target} by {var} deciles",
                                    xaxis_title = f"{var} deciles",
                                    yaxis_title = f"Avg target")

    print("-"*50)
    print(f"VARIABLE {var}")
    print(f"\nCrossing with {target} (uncensored):")
    print(summary_uncensored)
    print(f"\nCrossing with {target} (censored):")
    print(summary_censored)
    print("\n")
    boxplot.show()
    print("\n")
    decplot.show()

def explore_cat(df, target, var):

    ## summarise data
    df_summary = df \
        .assign(cnt = 1) \
        .groupby(var) \
        .agg({target : "sum", "cnt" : "count"}) \
        .assign(avg_target = lambda x: np.round(x[target] / x["cnt"], 4)) \
        .reset_index()
    rename_dict = dict(zip(df_summary[var].cat.categories, [str(i) for i in df_summary[var].cat.categories]))
    df_summary[var] = df_summary[var].cat.rename_categories(rename_dict)

    ## make plot
    fig = make_subplots(specs = [[{"secondary_y" : True}]])
    fig.add_trace(
        go.Bar(x = df_summary[var],
               y = df_summary["cnt"],
               name = f"Count {var}",
               marker = dict(color = "#636EFA"))
    )
    fig.add_trace(
        go.Scatter(x = df_summary[var],
                   y = df_summary["avg_target"],
                   name = f"Avg {target}",
                   mode = "lines+markers",
                   yaxis = "y2",
                   line = dict(color = "navy", width = 3))
    )
    fig = fig.update_layout(title = f"{var} (categorized) vs {target}",
                            xaxis_title = var)
    fig.update_yaxes(title_text = "Count", secondary_y = False)
    fig.update_yaxes(title_text = f"Avg target", secondary_y = True)

    print("-"*50)
    print(f"VARIABLE {var}")
    print(df_summary)
    print("\n")
    fig.show()

def check_single_feat_performance(df, target, num_vars, cat_vars):
    X = df.drop(columns = [target])
    y = df[target]
    csi = CustomSimpleImputer(strategy="median")

    num_auc = calc_predictive_power(X = csi.fit_transform(X[num_vars]),
                                                          y = y,
                                                          scoring_func = roc_auc_score
                                   )
    cat_auc = calc_predictive_power(X = X[cat_vars],
                                    y = y,
                                    scoring_func = roc_auc_score
                                   )
    df_perf = pd.DataFrame({"feature" : num_vars, "auc" : num_auc})
    df_perf = df_perf \
        .append(pd.DataFrame({"feature" : cat_vars, "auc" : cat_auc})) \
        .reset_index(drop = True)

    fig = px.bar(df_perf.sort_values("auc", ascending = False),
                 x = "feature",
                 y = "auc")
    fig = fig.update_layout(title = "Single feature performance",
                            xaxis_title = "features",
                            yaxis_title = "AUC")
    fig.show()
    return df_perf