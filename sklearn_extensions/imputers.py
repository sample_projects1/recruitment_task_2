"""Custom missing values imputation classes"""
import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer

class CustomSimpleImputer(SimpleImputer):
    def __init__(self,
                 missing_values = np.nan,
                 strategy = "mean",
                 fill_value = None,
                 verbose = 0,
                 copy = True,
                 add_indicator = False):
        super().__init__(missing_values = missing_values,
                         strategy =strategy,
                         fill_value = fill_value,
                         verbose = verbose,
                         copy = copy,
                         add_indicator = add_indicator)

    def get_feature_names_out(self, input_features = None):
        return self.feature_names_in_

    def fit(self, X, y = None):
        super().fit(X = X, y = y)
        return self

    def transform(self, X, y = None) -> pd.DataFrame:
        X_new = super().transform(X = X)
        return pd.DataFrame(X_new, columns = self.feature_names_in_, dtype = float)
