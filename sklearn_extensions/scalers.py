"""Custom scaling classes"""
import pandas as pd
from sklearn.preprocessing import StandardScaler

class CustomStandardScaler(StandardScaler):
    def __init__(self):
        super().__init__()

    def transform(self, X, y = None) -> pd.DataFrame:
        X_new = super().transform(X = X)
        return pd.DataFrame(X_new, columns = self.feature_names_in_, dtype = float)

