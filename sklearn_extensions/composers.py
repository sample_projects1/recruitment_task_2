"""Module with composer extensions"""
import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer

class CustomColumnTransformer(ColumnTransformer):
    """
    """
    def __init__(self,
                 transformers,
                 type_changes = False,
                 default_dtype = np.float,
                 remainder = 'drop',
                 sparse_threshold = 0.3,
                 n_jobs = None,
                 transformer_weights = None,
                 verbose = False,
                 verbose_feature_names_out = False):
        super().__init__(transformers = transformers,
                         remainder = remainder,
                         sparse_threshold = sparse_threshold,
                         n_jobs = n_jobs,
                         transformer_weights = transformer_weights,
                         verbose = verbose,
                         verbose_feature_names_out = verbose_feature_names_out)

        self.type_changes = type_changes
        self.default_dtype = default_dtype
        self.dtypes_dict = None

    def _get_dtypes(self, X):
        if self.type_changes:
            self.dtypes_dict = {col:self.default_dtype for col in X.columns}
        else:
            self.dtypes_dict = X.dtypes.to_dict()
            for k,v in self.dtypes_dict.items():
                if isinstance(v, pd.CategoricalDtype):
                    self.dtypes_dict[k] = "category"

    def fit(self, X, y = None):
        self.dtypes_dict = self._get_dtypes(X = X)
        super().fit(X = X, y = y)

    def transform(self, X, y = None) -> pd.DataFrame:
        X_new = super().transform(X = X)

        df_new = pd.DataFrame(X_new, columns = super().get_feature_names_out(), index = X.index)
        df_new = df_new.astype(self.dtypes_dict)

        return df_new

    def fit_transform(self, X, y = None) -> pd.DataFrame:
        X_new = super().fit_transform(X = X, y = y)

        self.dtypes_dict = self._get_dtypes(X = X)

        df_new = pd.DataFrame(X_new, columns = super().get_feature_names_out(), index = X.index)
        df_new = df_new.astype(self.dtypes_dict)

        return df_new
