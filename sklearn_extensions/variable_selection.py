"""Module with variable selection classes"""
from abc import ABCMeta, abstractmethod
import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.pipeline import Pipeline
from sklearn_extensions.encoders import CustomOneHotEncoder

class ConstantsFilter(BaseEstimator, TransformerMixin):
    """
    Variable selector which removes variables 
    with most common value percentage above the specified {threshold}.

    Parameters:
    -----------
    threshold : float [0,1), optional(default = 0.99)
        Variables with most common value percentage above this threshold will be removed.
    """
    def __init__(self, threshold = 0.99):
        self.threshold = threshold
        self.feature_stats = None
        self.remove_cols = None

    def _get_mode_cnt(self, x : pd.Series) -> int:

        if len(x.value_counts()) > 0:
            return x.value_counts().iloc[0]
        else:
            return 0

    def fit(self, X : pd.DataFrame, y = None):

        self.feature_stats = pd.DataFrame(X.apply(self._get_mode_cnt) / X.shape[0]) \
            .reset_index() \
            .rename(columns = {"index" : "feature", 0 : "mode_fraction"}) \
            .assign(to_remove = lambda x: (x["mode_fraction"] > self.threshold).astype(int))

        self.remove_cols = self.feature_stats.loc[self.feature_stats["to_remove"] == 1, "feature"].tolist()
        return self

    def transform(self, X : pd.DataFrame, y = None) -> pd.DataFrame:

        return X.drop(columns = self.remove_cols)

class CorrelationsFilter(BaseEstimator, TransformerMixin):
    """
    This filter removes continous variables which absolute correlations with other variables 
    exceed the {threshold}.
    Firstly, correlation matrix is calculated acompanied with number of correlations exceeding the {threshold} 
    and mean absolute correlation per variable.
    Then the algorithm iteratively removes variable which has most high correlations and recalculates the statistics
    until all high correlations are removed. If more than 1 variable has most correlations 
    then the one which has the highest mean absolute correlation is removed.

    Parameters:
    -----------
    corr_method : str, optional(default = "spearman")
        Correlations calculation method to use. Available options are: "spearman", "pearson", "kendall".

    threshold : float [0,1), optional(default = 0.9)
        Correlations higher than this value will be considered as high.
    """
    def __init__(self, threshold = 0.9, corr_method = "spearman"):
        self.corr_method = corr_method
        self.threshold = threshold
        self.corr_stats = None
        self.corr_matrix = None
        self.remove_cols = []

    def _calc_stats(self, corr_matrix : pd.DataFrame) -> pd.DataFrame:

        corr_stats = pd.concat([corr_matrix.apply(np.mean),
                                corr_matrix.apply(lambda x: (x > self.threshold)).sum()
                               ],
                               axis = 1)
        corr_stats = corr_stats \
            .rename(columns = {0 : "mean_abs_corr", 1 : "n_correlations"})

        return corr_stats

    def _check_if_correlated(self, corr_matrix : pd.DataFrame) -> bool:

        n_high_correlations = corr_matrix \
            .apply(lambda x: (x > self.threshold)) \
            .sum() \
            .sum()

        return n_high_correlations > 0

    def fit(self, X : pd.DataFrame, y = None):

        corr_matrix = X \
            .select_dtypes(exclude=["object", "category", "bool"]) \
            .corr(method = self.corr_method) \
            .abs()
        np.fill_diagonal(a = corr_matrix.values, val = np.nan)
        self.corr_matrix = corr_matrix.copy()

        self.corr_stats = self._calc_stats(corr_matrix = self.corr_matrix)

        corr_bool = self._check_if_correlated(corr_matrix = corr_matrix)
        while corr_bool:
            col_to_remove = self._calc_stats(corr_matrix = corr_matrix) \
                .sort_values(["n_correlations", "mean_abs_corr"], ascending = False) \
                .index[0]
            corr_matrix = corr_matrix \
                .drop(index = col_to_remove) \
                .drop(columns = col_to_remove)
            self.remove_cols.append(col_to_remove)
            corr_bool = self._check_if_correlated(corr_matrix = corr_matrix)
        return self
            
    def transform(self, X : pd.DataFrame, y = None) -> pd.DataFrame:

        return X.drop(columns = self.remove_cols)

def calc_predictive_power(X : pd.DataFrame, y : pd.Series, scoring_func : callable) -> np.array:
    """
    Calculates univariate predictive power of a variable measured by {scoring_func}.
    Meant to be used with classes like sklearn.feature_selection.SelectPercentile etc.
    
    Parameters:
    -----------
    X : pd.DataFrame
        Dataframe with X-variables.

    y : pd.Series
        Series with true Y values.

    scoring_func : callable
        Scoring function which measures model quality.
        Can be any callable which requires only `y_true` and `y_score` arguments (like `roc_auc_score` for example).

    Returns:
    -----------
    np.array
        Array of scores produces by {scoring_func}. One for each column from {X}.
    """
    scores = []

    for col in X.columns:
        data = X[[col]]

        if isinstance(data[col].dtype, pd.CategoricalDtype):
            model = Pipeline(steps = [("transform", CustomOneHotEncoder(drop = "first", sparse = True)),
                                      ("logit", LogisticRegression(class_weight = "balanced"))
                                     ]
                            )
        else:
            model = Pipeline(steps = [("transform", PolynomialFeatures(degree = 3, include_bias = False)),
                                      ("scaler", StandardScaler()),
                                      ("logit", LogisticRegression(class_weight = "balanced"))
                                     ]
                            )

        model.fit(X = data, y = y)
        y_score = model.predict_proba(X = data)[:, 1]

        score = scoring_func(y_true = y, y_score = y_score)
        scores.append(score)

    return np.array(scores)


class BaseFeatureSelector(TransformerMixin, BaseEstimator, metaclass = ABCMeta):
    """
    Base class for feature selection.
    Applies {score_func} to X and y to obtain score for each column in X.
    In the transform phase all columns from {remove_cols} list are removed from X.
    find_cols_to_remove method has to be implemented to determine {remove_cols} list.
    """
    def __init__(self, score_func):
        self.score_func = score_func
        self.feature_names_in_ = []
        self.feature_stats = pd.DataFrame()
        self.remove_cols = []

    @abstractmethod
    def find_cols_to_remove(self):
        """Determine which columns should be removed"""

    def get_feature_names_out(self, input_features = None):
        return [col for col in self.feature_names_in_ if col not in self.remove_cols]

    def fit(self, X : pd.DataFrame, y : pd.Series):
        """Run score function on (X, y) and get the appropriate features.
        Parameters
        ----------
        X : pd.DataFrame
            The training input samples.
        y : pd.Series
            The target values (class labels in classification, real numbers in
            regression).
        Returns
        -------
        self : object
            Returns the instance itself.
        """
        self.feature_names_in_ = X.columns.tolist()

        if not callable(self.score_func):
            raise TypeError(
                "The score function should be a callable, %s (%s) was passed."
                % (self.score_func, type(self.score_func))
            )

        score_func_ret = self.score_func(X, y)
        if isinstance(score_func_ret, (list, tuple)):
            scores, pvalues = score_func_ret
            self.feature_stats = pd.DataFrame({"feature" : X.columns,
                                               "score" : scores,
                                               "pvalue" : pvalues}
                                             )
        else:
            scores = score_func_ret
            self.feature_stats = pd.DataFrame({"feature" : X.columns,
                                               "score" : scores,
                                               "pvalue" : np.nan}
                                             )

        self.find_cols_to_remove()

        return self

    def transform(self, X : pd.DataFrame, y = None) -> pd.DataFrame:
        return X.drop(columns = self.remove_cols)


class CustomSelectPercentile(BaseFeatureSelector):
    """
    Selects {percentile} of features with highest scores.
    """
    def __init__(self, score_func : callable, percentile = 10):
        super().__init__(score_func = score_func)
        self.percentile = percentile

    def find_cols_to_remove(self):
        percentile_value = np.percentile(a = self.feature_stats["score"], q = 100 - self.percentile)
        self.feature_stats["to_remove"] = np.where(self.feature_stats["score"] < percentile_value, 1, 0)
        self.remove_cols = self.feature_stats \
            .query("to_remove == 1")["feature"] \
            .tolist()


class CustomSelectKBest(BaseFeatureSelector):
    """
    Selects {k} features with highest scores.
    """
    def __init__(self, score_func : callable, k = 10):
        super().__init__(score_func = score_func)
        self.k = k

    def find_cols_to_remove(self):
        cutoff = sorted(self.feature_stats["score"], reverse = True)[self.k - 1]
        self.feature_stats["to_remove"] = np.where(self.feature_stats["score"] < cutoff, 1, 0)
        self.remove_cols = self.feature_stats \
            .query("to_remove == 1")["feature"] \
            .tolist()


class CustomSelectCumScorePct(BaseFeatureSelector):
    """
    Selects features which scores' account for {pct} of the cumulative score sum.
    """
    def __init__(self, score_func : callable, pct = 0.9):
        super().__init__(score_func = score_func)
        self.pct = pct

    def find_cols_to_remove(self):
        self.feature_stats = self.feature_stats.sort_values("score", ascending = False)
        self.feature_stats["cumulative_score"] = self.feature_stats["score"].cumsum()
        score_sum = self.feature_stats["score"].sum()
        self.feature_stats["cumulative_pct"] = self.feature_stats["cumulative_score"] / score_sum
        self.feature_stats["to_remove"] = np.where(self.feature_stats["cumulative_pct"] <= self.pct, 0, 1)
        self.remove_cols = self.feature_stats \
            .query("to_remove == 1")["feature"] \
            .tolist()

