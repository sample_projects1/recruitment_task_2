"""Module with custom categorical features encoders"""
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder
from category_encoders import WOEEncoder

class CustomOneHotEncoder(OneHotEncoder):
    """
    Sklearn OneHotEncoder which captures names of incoming features.
    """
    def __init__(self,
                 categories = "auto",
                 drop = None,
                 sparse = False,
                 dtype = float,
                 handle_unknown = "error"):
        super().__init__(categories = categories,
                         drop = drop,
                         sparse = sparse,
                         dtype = dtype,
                         handle_unknown = handle_unknown)
        self.columns = None

    def fit(self, X, y = None):

        self.columns = X.columns.tolist()
        super().fit(X = X, y = y)

        return self

    def get_feature_names(self, input_features = None):

        if len(input_features) != len(self.categories_):
            input_features = self.columns

        return super().get_feature_names(input_features)


class CustomOrdinalEncoder(OrdinalEncoder):
    """
    Sklearn OrdinalEncoder which returns pandas DataFrame instead of numpy array.
    """
    def __init__(self,
                 categories = "auto",
                 dtype = int):
        super().__init__(categories = categories,
                         dtype = dtype)
        self.columns = None

    def fit(self, X, y = None):

        self.columns = X.columns.tolist()
        super().fit(X = X, y = y)

        return self

    def transform(self, X) -> pd.DataFrame:
        
        X_new = super().transform(X = X)
        df_new = pd.DataFrame(X_new, columns = self.columns, dtype = "category")
        
        return df_new

    def get_feature_names_out(self, input_features = None):
        return self.feature_names_in_


class CustomWOEEncoder(WOEEncoder):
    """
    """
    def __init__(self, 
                 verbose = 0, 
                 cols = None, 
                 drop_invariant = False, 
                 return_df=True, 
                 handle_unknown='value', 
                 handle_missing='value', 
                 random_state=None, 
                 randomized=False, 
                 sigma=0.05, 
                 regularization=1.0):
        super().__init__(verbose=verbose, 
                         cols=cols, 
                         drop_invariant=drop_invariant, 
                         return_df=return_df, 
                         handle_unknown=handle_unknown, 
                         handle_missing=handle_missing, 
                         random_state=random_state, 
                         randomized=randomized, 
                         sigma=sigma, 
                         regularization=regularization)
        self.columns = None

    def get_feature_names_out(self, input_features = None):
        return self.columns

    def fit(self, X, y = None):

        self.columns = X.columns.tolist()
        super().fit(X = X, y = y)

        return self


class UnknownCategoriesRemover(BaseEstimator, TransformerMixin):
    """In the transform phase removes categories unseen during fit and replaces them with mode"""
    def __init__(self, cols=None):
        self.cols = cols
        self.cats_dict = {}
        self.defaults_dict = {}

    def get_feature_names_out(self, input_features = None):
        return self.feature_names_in_

    def fit(self, X : pd.DataFrame, y = None):

        self.feature_names_in_ = X.columns.tolist()

        if self.cols is None:
            self.cols = X.select_dtypes("category").columns.tolist()

        for c in self.cols:
            self.cats_dict.update({c : X[c].cat.categories.tolist()})

        dct = X[self.cols].mode().T.to_dict(orient = "index")
        self.defaults_dict = {k:v.get(0) for k,v in dct.items()}

        return self

    def transform(self, X : pd.DataFrame, y = None) -> pd.DataFrame:

        X_new = X.copy()

        for c in self.cols:
            known_cats = self.cats_dict.get(c)
            cats = X_new[c].cat.categories.tolist()
            if len(set(known_cats + cats)) > len(known_cats):
                new_cats = [c for c in cats if c not in known_cats]
                X_new[c] = X_new[c].cat.remove_categories(new_cats)
                X_new[c] = X_new[c].fillna(self.defaults_dict.get(c))

        return X_new
