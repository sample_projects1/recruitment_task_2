"""Classes for calculating new variables"""
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.cluster import KMeans
from sklearn.metrics import calinski_harabasz_score
from sklearn.preprocessing import StandardScaler

class KMeansAugmenter(BaseEstimator, TransformerMixin):
    """
    Performs different kmeans clusterings, selects the best, and adds cluster label and distances to X.
    """
    def __init__(self, possible_clusters = [3, 4, 5, 6]):
        self.possible_clusters = possible_clusters
        self.n_features_in_ = None
        self.feature_names_in_ = None
        self.estimators = []
        self.chs_list = []
        self.best_clustering_idx = None
        self.best_model = None
        self.n_clusters = None
        self.scaler = StandardScaler()

    def _calc_dist_matrix(self, X):
        lst = []
        for i in range(len(self.best_model.cluster_centers_)):
            lst.append(np.linalg.norm(X.values - self.best_model.cluster_centers_[i],
                                      axis = 1)
                      )
        return np.stack(lst, axis = 1)

    def get_feature_names_out(self, input_features = None):
        dist_vars = [f"d_{i}" for i in range(self.n_clusters)]
        return self.feature_names_in_ + ["cluster_label"] + dist_vars

    def fit(self, X, y = None):

        self.feature_names_in_ = X.columns.tolist()

        for k in self.possible_clusters:
            kmeans = KMeans(n_clusters = k, random_state = 1993)
            kmeans = kmeans.fit(X)
            self.estimators.append(kmeans)
            self.chs_list.append(calinski_harabasz_score(X = X, labels = kmeans.labels_))

        self.best_clustering_idx = np.argmax(self.chs_list)
        self.best_model = self.estimators[self.best_clustering_idx]
        self.n_clusters = self.possible_clusters[self.best_clustering_idx]
        
        dist_mat = self._calc_dist_matrix(X)
        self.scaler = self.scaler.fit(dist_mat)

        return self

    def transform(self, X, y = None) -> pd.DataFrame:

        X_new = X.copy()

        X_new["cluster_label"] = pd.Series(self.best_model.predict(X), dtype = "category")
        dist_mat = self._calc_dist_matrix(X)
        dist_mat = self.scaler.transform(dist_mat)
        df_dist = pd.DataFrame(dist_mat,
                               columns = [f"d_{i}" for i in range(dist_mat.shape[1])])
        X_new = pd.concat([X_new, df_dist], axis = 1)
        return X_new

