"""Module with transformer for long tail removal in categorical features"""
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

class LongTailRemover(BaseEstimator, TransformerMixin):
    """
    Transformer for removing long tail from categorical features.
    For each feature from {cat_cols} feature list 
    levels with less than {threshold} percentage of observations are replaced.

    If percentage share of levels to be removed is lower than {threshold} 
    then they are appended to the last valid level. 
    Otherwise they are replaced with -999 or "other".

    Parameters:
    -----------
    cat_cols : list, optional(default = None)
        List of features for which long tail removal should be performed.
        If None then all Category dtype columns will be used.

    min_levels : int, optional(default = 3)
        Minimum unique levels of the categorical variable to be processed.
        Used if {cat_cols} is None.

    exclude_cols : list, optional(default = [])
        List of features to be excluded from long tail removal process.

    threshold : float [0,1), optional(default = 0.01)
        Minimum percentage of observations which won't be considered as long tail.
    """
    def __init__(self,
                 cat_cols = None,
                 min_levels = 3,
                 exclude_cols = [],
                 threshold = 0.01):

        self.cat_cols = cat_cols
        self.min_levels = min_levels
        self.exclude_cols = exclude_cols
        self.threshold = threshold
        self.feature_names_in_ = None
        self.replacement_dict = dict()
        self.remove_cols = []
        self.summary = dict()

    def _get_cat_cols(self, X):
        """
        Function to extract categorical columns to process.
        """
        unique_count = X \
            .select_dtypes("category") \
            .drop(columns = self.exclude_cols) \
            .nunique(dropna = True)
        self.cat_cols = list(unique_count[unique_count >= self.min_levels].index)

    def get_feature_names_out(self, input_features = None):
        return self.feature_names_in_

    def fit(self, X, y = None):

        self.feature_names_in_ = X.columns.tolist()

        if self.cat_cols is None:
            self._get_cat_cols(X = X)

        if self.cat_cols:
            for col in self.cat_cols:
                cat_summary = X[col].value_counts(normalize = True)
                self.summary.update({f"{col}" : cat_summary})
                mask = cat_summary > self.threshold
                if not all(mask) and any(mask):
                    valid_levels = cat_summary[mask].index.tolist()
                    other_valid_flg = np.sum(cat_summary[~mask]) > self.threshold
                    int_bool = X[col].cat.categories.dtype == int
                    float_bool = X[col].cat.categories.dtype == float

                    if other_valid_flg:
                        if int_bool:
                            replace_level = -999
                        elif float_bool:
                            replace_level = 999.0
                        else:
                            replace_level = "other"
                    else:
                        replace_level = valid_levels[-1]

                    self.replacement_dict.update({f"{col}" : {"valid_levels" : valid_levels,
                                                              "replace_level" : replace_level}
                                                 })
                if not any(mask):
                    self.remove_cols.append(col)

        return self

    def transform(self, X, y = None) -> pd.DataFrame:

        X_new = X.copy()

        if self.replacement_dict:
            for k,v in self.replacement_dict.items():
                X_new[k] = X_new[k].cat.set_categories(v.get("valid_levels"))
                if v.get("replace_level") not in v.get("valid_levels"):
                    X_new[k] = X_new[k].cat.add_categories(v.get("replace_level"))
                X_new[k] = X_new[k].fillna(v.get("replace_level"))

        return X_new
