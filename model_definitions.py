from sklearn.pipeline import Pipeline
from sklearn.compose import make_column_selector

from sklearn_extensions.encoders import CustomOrdinalEncoder, UnknownCategoriesRemover
from sklearn_extensions.long_tail_remover import LongTailRemover
from sklearn_extensions.composers import CustomColumnTransformer
from sklearn_extensions.scalers import CustomStandardScaler
from sklearn_extensions.augmenters import KMeansAugmenter

from modeling.custom_models import CustomCatBoostClassifier

## cb_model
cat_transformer = Pipeline(steps = [("unknown_remover", UnknownCategoriesRemover()),
                                    ("lt_remover", LongTailRemover(threshold = 0.01)),
                                    ("oe", CustomOrdinalEncoder())
                                   ]
                          )
num_transformer = Pipeline(steps = [("scaler", CustomStandardScaler())])

type_preprocessor = CustomColumnTransformer(
    transformers = [("num", num_transformer, make_column_selector(dtype_exclude = "category")),
                    ("cat", cat_transformer, make_column_selector(dtype_include = "category"))],
    verbose_feature_names_out = False
    )

cb_model = Pipeline(steps = [("preproc", type_preprocessor),
                             ("classifier", CustomCatBoostClassifier(iterations = 300,
                                                                     es_enabled = True,
                                                                     early_stopping_rounds = 10,
                                                                     auto_class_weights = "Balanced",
                                                                     random_seed = 1993,
                                                                     thread_count = 3))
                             ]
                   )
