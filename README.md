# recruitment_task_2

## order of steps
1. clone the repo.
2. create virtualenv from the `requirements.txt` file.
3. put CSV files in the `./data` directory.
4. `profile_data.ipynb` - notebook with initial data exploration. It generates **html** reports `main_data_profile.html` and `blind_data_profile.html`.
5. `eda_vs_target.ipynb` - analysis of relationships between predictive variables and the target.
6. `initial_data_prep.ipynb` - preparation of data before modeling. Type conversions, variable filtering etc.
7. `tune_hyperparams.ipynb` - hyperparameter tuning, metrics logging.
8. `finalize_models.ipynb` - final models estimation and validation, explanation of final models' predictions.