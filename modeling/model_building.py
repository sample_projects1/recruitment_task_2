""""""
import sklearn
from modeling.utils import flatten_dict

class ModelBuilder:
    def __init__(self,
                 model_definition,
                 early_stopping : bool,
                 early_stopping_param : str = None,
                 **predefined_params):
        self.model = model_definition
        self.early_stopping = early_stopping
        self.early_stopping_param = early_stopping_param
        self.predefined_params = predefined_params

    def _make_dict(self, generated_params_dict : dict) -> dict:
        params_dict = flatten_dict(generated_params_dict)
        params_dict.update(**self.predefined_params)
        return params_dict

    def set_params(self, generated_params_dict : dict):
        params_dict = self._make_dict(generated_params_dict)
        return self.model.set_params(**params_dict)

