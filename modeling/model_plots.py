"""Module with model validation plotting functions"""
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from typing import Union, List, Dict, Tuple
from sklearn.metrics import roc_auc_score, roc_curve, average_precision_score, precision_recall_curve

def plot_cv_auc(data : List[Dict[Union[np.array, pd.Series], Union[np.array, pd.Series]]],
                figsize : Tuple[int, int]):
    font = {'family' : 'DejaVu Sans',
            'weight' : 'bold',
            'size'   : 15}

    matplotlib.rc('font', **font)

    fig = plt.figure(figsize = figsize)
    lw = 2
    for i in range(len(data)):
        fpr, tpr, _ = roc_curve(data[i]["y_true"], data[i]["y_pred_proba"])
        auc_val = roc_auc_score(data[i]["y_true"], data[i]["y_pred_proba"])
        plt.plot(
            fpr,
            tpr,
            lw = lw,
            label = "AUC = %0.3f" % auc_val
        )
    plt.plot([0, 1], [0, 1], color = "navy", lw = lw, linestyle = "--")
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("ROC Curve")
    plt.legend(loc = "lower right")

    return fig

def plot_cv_auprc(data : List[Dict[Union[np.array, pd.Series], Union[np.array, pd.Series]]],
                  figsize : Tuple[int, int]):
    fig = plt.figure(figsize = figsize)
    lw = 2
    for i in range(len(data)):
        prec, rec, _ = precision_recall_curve(data[i]["y_true"], data[i]["y_pred_proba"])
        auc_val = roc_auc_score(data[i]["y_true"], data[i]["y_pred_proba"])
        plt.plot(
            rec,
            prec,
            lw = lw,
            label = "AUC = %0.2f" % auc_val
        )
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.ylabel("Precision")
    plt.xlabel("Recall")
    plt.title("Precision-Recall Curve")
    plt.legend(loc = "upper right")

    return fig
