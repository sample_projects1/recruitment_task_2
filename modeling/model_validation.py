"""Module with model validation utilities"""
import sklearn
import numpy as np
import pandas as pd
from copy import deepcopy
from typing import Tuple, Union
from itertools import chain
from time import time
from sklearn.metrics import roc_auc_score, average_precision_score, fbeta_score

def optimize_metric(y_true : Union[pd.Series, np.array],
                    y_pred_proba : np.array,
                    func : callable,
                    n_steps = 201,
                    **kwargs) -> Tuple[float, float]:
    """Explores possible probability cutoffs looking for maximum value of {func}"""
    cutoffs, values = [], []
    for cutoff in np.linspace(0, 1, n_steps):
        cutoffs.append(cutoff)
        y_pred = np.where(y_pred_proba >= cutoff, 1, 0)
        values.append(func(y_true, y_pred, **kwargs))
    i_best = np.argmax(values)
    return values[i_best], cutoffs[i_best]


class CrossValidator:
    def __init__(self, X : pd.DataFrame, y : pd.Series, cv, validating_func : callable):
        self.X = X
        self.y = y
        self.cv = cv
        self.validating_func = validating_func
        self.results = None
        self._standard_cols = ["fit_time_mean", "fit_time_std",
                              "predict_time_mean", "predict_time_std",
                              "estop_iter_mean", "estop_iter_std"]
        self._results_agg = None
        self.prediction_data = []

    @staticmethod
    def dict_col_to_df(dict_col : pd.Series, suffix = "") -> pd.DataFrame:
        df = dict_col.apply(pd.Series)
        df.columns = [c + suffix for c in df.columns]
        return df

    @property
    def results_agg(self) -> pd.DataFrame:
        results_agg = pd.DataFrame(self.results.mean()) \
            .T \
            .join(pd.DataFrame(self.results.std()) \
                      .T,
                  lsuffix = "_mean",
                  rsuffix = "_std"
            )
        results_agg = results_agg \
            .filter(chain(self._standard_cols,
                          sorted([c for c in results_agg.columns if c not in self._standard_cols])
                         )
                   )
        return results_agg

    def cross_validate(self,
                       model,
                       early_stopping = False,
                       early_stopping_param : str = None,
                       fit_params = {},
                       verbose = False,
                      ):
        if early_stopping:
            estop_iter = []
        else:
            estop_iter = [np.nan for i in range(self.cv.n_splits)]
        fit_time, predict_time, metrics_train, metrics_test = [], [], [], []
        i = 1
        for train_index, test_index in self.cv.split(self.X, self.y):
            if verbose:
                print(f"Iteration {i}/{self.cv.n_splits} ...")
            X_train = self.X.iloc[train_index]
            y_train = self.y.iloc[train_index]
            X_test = self.X.iloc[test_index]
            y_test = self.y.iloc[test_index]
            cv_model = deepcopy(model)

            t0 = time()
            cv_model.fit(X = X_train, y = y_train, **fit_params)
            fit_time.append(np.round(time() - t0, 2))
            if early_stopping:
                estop_iter.append(getattr(cv_model["classifier"], early_stopping_param))

            t0 = time()
            y_train_proba = cv_model.predict_proba(X_train)[:,1]
            predict_time.append(np.round(time() - t0, 2))
            y_test_proba = cv_model.predict_proba(X_test)[:,1]

            metrics_train.append(self.validating_func(y_train, y_train_proba))
            metrics_test.append(self.validating_func(y_test, y_test_proba))
            
            self.prediction_data.append({"y_true" : y_test, "y_pred_proba" : y_test_proba})

            i += 1

        results = pd.DataFrame({"fit_time" : fit_time,
                                "predict_time" : predict_time,
                                "estop_iter" : estop_iter,
                                "metrics_train" : metrics_train,
                                "metrics_test" : metrics_test
                               })
        self.results = results \
            .drop(columns = ["metrics_train", "metrics_test"]) \
            .join(self.dict_col_to_df(results["metrics_train"], suffix = "_train")) \
            .join(self.dict_col_to_df(results["metrics_test"], suffix = "_test"))


def validate_model(y_true : Union[pd.Series, np.array], y_pred_proba : np.array) -> dict:
    auc = roc_auc_score(y_true = y_true, y_score = y_pred_proba)
    auprc = average_precision_score(y_true = y_true, y_score = y_pred_proba)
    f1_score = optimize_metric(y_true, y_pred_proba, func = fbeta_score, beta = 1)[0]
    return {"AUC" : auc, "AUPRC" : auprc, "F1" : f1_score}