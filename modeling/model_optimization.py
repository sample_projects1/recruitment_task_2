"""Module with model optimization utilities"""
import os
import shutil
import optuna
import sklearn
import mlflow
from typing import Type
from copy import deepcopy
from modeling.model_building import ModelBuilder
from modeling.model_validation import CrossValidator
from modeling.model_plots import plot_cv_auc

class Objective:
    def __init__(self,
                 model_builder : Type[ModelBuilder],
                 cross_validator : Type[CrossValidator],
                 optim_params : dict,
                 fixed_params : dict = {},
                 fit_params : dict = {},
                 run_name : str = None,
                 config_path : str = None):
        self.model_builder = model_builder
        self.cross_validator = cross_validator
        self.optim_params = optim_params
        self.fixed_params = {k:v for k,v in fixed_params.items() if v}
        self.fit_params = fit_params
        self.model_params = {}
        self.run_name = run_name
        self.config_path = config_path

    def _prep_params(self, trial : optuna.trial.Trial):
        suggested_params = {}
        for method, params in self.optim_params.items():
            for param_name, value in params.items():
                if param_name not in self.fixed_params:
                    trial_method = getattr(trial, method)
                    suggested_params[param_name] = trial_method(param_name, **value)

        dct = self.fixed_params.copy()
        dct.update(suggested_params)
        self.model_params = dct

    def build_model(self, trial : optuna.trial.Trial):
        self._prep_params(trial)
        model = self.model_builder.set_params(self.model_params)
        return model

    def __call__(self, trial : optuna.trial.Trial) -> float:

        shutil.rmtree("./artifacts/", ignore_errors = True)
        os.makedirs("./artifacts/")

        run_name = f"{trial.number}"
        if self.run_name:
            run_name = f"{self.run_name}_{run_name}"

        print("-"*50)
        print(f"{run_name}")

        with mlflow.start_run(run_name = run_name):
            model = self.build_model(trial)
            mlflow.log_params(self.model_params)
            cross_validator = deepcopy(self.cross_validator)
            cross_validator.cross_validate(model = model,
                                                early_stopping = self.model_builder.early_stopping,
                                                early_stopping_param = self.model_builder.early_stopping_param,
                                                fit_params = self.fit_params,
                                                verbose = True)
            df_results = cross_validator.results
            df_results_agg = cross_validator.results_agg
            results_dict = df_results_agg.to_dict(orient = "records")[0]
            metrics_dict = {k:v for k,v in results_dict.items() if k.startswith(("AUC", "AUPRC", "F", "estop_iter"))}
            mlflow.log_metrics(metrics_dict)

            auc_fig = plot_cv_auc(data = cross_validator.prediction_data, figsize = (13, 13))
            auc_fig.savefig("./artifacts/cv_auc_plot.jpeg")
            if self.config_path:
                shutil.copy(self.config_path, "./artifacts/config.yaml")
            mlflow.log_artifacts("./artifacts/")
        return df_results_agg["AUC_test_mean"][0]
