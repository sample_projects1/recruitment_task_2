"""Custom models implementation"""
from sklearn.model_selection import train_test_split
from lightgbm import LGBMClassifier, early_stopping
from catboost import CatBoostClassifier

class CustomLGBMClassifier(LGBMClassifier):
    """
    """
    def __init__(self,
                 es_enabled = False,
                 early_stopping_rounds = None,
                 eval_metric = None,
                 test_size = 0.1,
                 **init_params):
        super().__init__(**init_params)
        self.es_enabled = es_enabled
        self.early_stopping_rounds = early_stopping_rounds
        self.eval_metric = eval_metric
        self.test_size = test_size
        self.eval_class_weight = init_params.get("class_weight", None)

    def get_params(self, deep=True):
        params = self.__dict__
        params = {k:v for k,v in params.items() if not k.startswith("_")}
        return params

    def fit(self, X, y):
        if self.es_enabled:
            x_train, x_val, y_train, y_val = train_test_split(X, y, test_size = self.test_size)
            es_callback = early_stopping(stopping_rounds = self.early_stopping_rounds)
            return super().fit(X = x_train,
                               y = y_train,
                               eval_set = [(x_val, y_val)],
                               eval_names = ["inner_validation_set"],
                               eval_class_weight = None,
                               eval_metric = self.eval_metric,
                               callbacks = [es_callback])
        else:
            return super().fit(X = X, y = y)


class CustomCatBoostClassifier(CatBoostClassifier):
    """
    """
    def __init__(self,
                 es_enabled = False,
                 test_size = 0.1,
                 **init_params):
        super().__init__(**init_params)
        self.es_enabled = es_enabled
        self.test_size = test_size
        self.cat_cols = None

    def __deepcopy__(self, *args):
        return CustomCatBoostClassifier(es_enabled = self.es_enabled,
                                        test_size = self.test_size,
                                        **self._init_params)

    def get_params(self, deep=True):
        params = self.__dict__
        params = {k:v for k,v in params.items() if not k.startswith("_")}
        params.update(self.__dict__["_init_params"])
        return params

    def fit(self, X, y):
        self.cat_cols = X.select_dtypes(include = "category").columns.tolist()

        if self.es_enabled:
            x_train, x_val, y_train, y_val = train_test_split(X, y, test_size = self.test_size)
            return super().fit(X = x_train,
                               y = y_train,
                               eval_set = (x_val, y_val),
                               cat_features = self.cat_cols)
        else:
            return super().fit(X = X, y = y, cat_features = self.cat_cols)

